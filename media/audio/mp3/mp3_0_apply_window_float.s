# ffmpeg lgpl 2.1 or later

	.file	"mpegaudiodsp_float.c"
	.machine power9
	.abiversion 2
	.section	".text"
	.align 2
	.p2align 4,,15
	.globl ff_mpadsp_apply_window_float
	.type	ff_mpadsp_apply_window_float, @function
ff_mpadsp_apply_window_float:
.LCF0:
0:	addis 2,12,.TOC.-.LCF0@ha
	addi 2,2,.TOC.-.LCF0@l
	.localentry	ff_mpadsp_apply_window_float,.-ff_mpadsp_apply_window_float
	mflr 0
	stfd 29,-24(1)
	stfd 30,-16(1)
	stfd 31,-8(1)
	std 27,-184(1)
	std 28,-176(1)
	std 29,-168(1)
	std 30,-160(1)
	std 31,-152(1)
	mr 30,4
	mr 31,3
	mr 4,3
	mr 27,5
	stfd 14,-144(1)
	stfd 15,-136(1)
	stfd 16,-128(1)
	stfd 17,-120(1)
	stfd 18,-112(1)
	stfd 19,-104(1)
	stfd 20,-96(1)
	stfd 21,-88(1)
	stfd 22,-80(1)
	stfd 23,-72(1)
	stfd 24,-64(1)
	stfd 25,-56(1)
	stfd 26,-48(1)
	std 0,16(1)
	stfd 27,-40(1)
	stfd 28,-32(1)
	li 5,128
	stdu 1,-224(1)
	addi 3,3,2048
	mr 28,6
	mr 29,7
	bl memcpy
	nop
	lfs 12,512(30)
	lfs 8,576(31)
	lfs 3,64(31)
	lfiwax 9,0,27
	lfs 11,0(30)
	lfs 5,832(31)
	lfs 6,1088(31)
	lfs 30,768(30)
	lfs 31,1024(30)
	lfs 2,1792(30)
	lfs 4,256(30)
	lfs 10,320(31)
	fmuls 29,12,8
	lfs 8,1856(31)
	mulli 11,29,124
	fcfids 9,9
	lfs 12,960(31)
	cmpdi 0,29,1
	addi 6,31,64
	fmuls 11,11,3
	lfs 7,1344(31)
	sldi 4,29,2
	fmuls 30,30,5
	lfs 5,640(30)
	fmuls 31,31,6
	lfs 6,896(30)
	add 7,28,4
	neg 3,4
	fmuls 10,4,10
	lfs 1,1280(30)
	add 11,28,11
	fmuls 2,2,8
	lfs 8,704(31)
	fadds 11,11,9
	lfs 0,1600(31)
	lfs 13,1536(30)
	lfs 4,192(31)
	lfs 3,128(30)
	lfs 9,1216(31)
	fmuls 6,6,12
	lfs 12,1472(31)
	fmuls 1,1,7
	lfs 7,448(31)
	fmuls 5,5,8
	lfs 8,1408(30)
	fadds 11,11,10
	lfs 10,1920(30)
	fmuls 13,13,0
	lfs 0,384(30)
	fmuls 3,3,4
	fmuls 8,8,12
	fadds 12,11,29
	fmuls 4,0,7
	lfs 7,1152(30)
	lfs 0,1728(31)
	fadds 12,12,30
	fmuls 7,7,9
	lfs 9,1664(30)
	fadds 12,12,31
	fmuls 9,9,0
	lfs 0,1984(31)
	fadds 12,12,1
	fadds 12,12,13
	fmuls 11,10,0
	fadds 12,12,2
	fsubs 12,12,3
	fsubs 0,12,4
	fsubs 0,0,5
	fsubs 0,0,6
	fsubs 0,0,7
	fsubs 0,0,8
	fsubs 0,0,9
	fsubs 0,0,11
	stfs 0,0(28)
	bne 0,.L10
	li 5,15
	mr 9,30
	addi 10,30,128
	addi 8,31,192
	mr 12,7
	mtctr 5
	.p2align 4,,15
.L5:
	lfsu 0,-4(10)
	lfsu 15,4(6)
	lfsu 9,-4(8)
	lfs 26,260(9)
	lfs 12,4(9)
	addi 9,9,4
	lfs 24,256(10)
	lfs 25,512(10)
	lfs 14,256(6)
	fmuls 0,15,0
	lfs 6,512(6)
	lfs 23,1280(8)
	lfs 7,768(6)
	lfs 8,1024(6)
	lfs 16,1280(6)
	lfs 17,1536(6)
	lfs 18,1792(6)
	lfs 19,256(8)
	fneg 0,0
	lfs 20,512(8)
	fmuls 12,15,12
	fmuls 24,14,24
	lfs 21,768(8)
	fmuls 26,14,26
	lfs 22,1024(8)
	lfs 10,1536(8)
	lfs 11,1792(8)
	fmuls 25,6,25
	lfs 27,768(10)
	lfs 28,1024(10)
	lfs 29,1280(10)
	lfs 30,1536(10)
	lfs 31,1792(10)
	lfs 1,128(10)
	lfs 13,384(10)
	lfs 2,640(10)
	lfs 3,896(10)
	lfs 4,1152(10)
	lfs 5,1408(10)
	fsubs 0,0,24
	fadds 26,26,12
	lfs 24,512(9)
	lfs 12,1024(9)
	fmuls 27,7,27
	fmuls 28,8,28
	fmuls 29,16,29
	fmuls 30,17,30
	fmuls 31,18,31
	fmuls 1,9,1
	lfs 15,1664(10)
	fmuls 13,19,13
	fmuls 2,20,2
	fsubs 0,0,25
	lfs 25,768(9)
	fmuls 3,21,3
	fmuls 5,23,5
	fmuls 4,22,4
	fmuls 6,6,24
	fmuls 8,8,12
	lfs 12,1792(9)
	lfs 24,1408(9)
	fmuls 14,10,15
	lfs 15,1920(10)
	fsubs 0,0,27
	lfs 27,1536(9)
	fmuls 7,7,25
	lfs 25,1280(9)
	fadds 26,26,6
	lfs 6,640(9)
	fmuls 18,18,12
	lfs 12,128(9)
	fmuls 24,23,24
	fsubs 0,0,28
	lfs 28,384(9)
	fmuls 15,11,15
	fmuls 17,17,27
	lfs 27,1920(9)
	fadds 26,26,7
	fmuls 16,16,25
	fmuls 20,20,6
	fmuls 9,9,12
	lfs 12,896(9)
	fsubs 0,0,29
	fmuls 19,19,28
	fadds 26,26,8
	fmuls 11,11,27
	fsubs 30,0,30
	fmuls 21,21,12
	lfs 12,1152(9)
	fadds 16,26,16
	fsubs 31,30,31
	fmuls 22,22,12
	lfs 12,1664(9)
	fadds 17,16,17
	fsubs 1,31,1
	fadds 18,17,18
	fmuls 10,10,12
	fsubs 13,1,13
	fsubs 9,18,9
	fsubs 2,13,2
	fsubs 23,9,19
	fsubs 3,2,3
	fsubs 23,23,20
	fsubs 4,3,4
	fsubs 23,23,21
	fsubs 5,4,5
	fsubs 23,23,22
	fsubs 14,5,14
	fsubs 23,23,24
	fsubs 15,14,15
	fsubs 10,23,10
	fsubs 11,10,11
	stfs 11,0(12)
	stfs 15,0(11)
	add 12,12,4
	add 11,11,3
	bdnz .L5
.L4:
	lfs 31,128(31)
	lfs 12,192(30)
	lfs 5,384(31)
	lfs 0,448(30)
	lfs 1,640(31)
	lfs 6,704(30)
	lfs 13,896(31)
	lfs 7,960(30)
	lfs 2,1152(31)
	lfs 8,1216(30)
	lfs 3,1408(31)
	lfs 9,1472(30)
	fmuls 12,12,31
	lfs 4,1664(31)
	addi 1,1,224
	li 9,0
	fmuls 0,0,5
	lfs 10,1728(30)
	mulli 29,29,60
	fmuls 6,6,1
	lfs 5,1920(31)
	ld 0,16(1)
	ld 28,-176(1)
	fmuls 7,7,13
	lfs 11,1984(30)
	ld 31,-152(1)
	ld 30,-160(1)
	fmuls 8,8,2
	lfd 14,-144(1)
	lfd 15,-136(1)
	lfd 16,-128(1)
	fneg 12,12
	fmuls 9,9,3
	lfd 17,-120(1)
	lfd 18,-112(1)
	mtlr 0
	lfd 19,-104(1)
	lfd 20,-96(1)
	lfd 21,-88(1)
	fsubs 0,12,0
	fmuls 10,10,4
	fmuls 11,11,5
	fsubs 0,0,6
	fsubs 0,0,7
	fsubs 0,0,8
	fsubs 0,0,9
	fsubs 0,0,10
	fsubs 0,0,11
	stfsx 0,7,29
	stw 9,0(27)
	ld 29,-168(1)
	ld 27,-184(1)
	lfd 22,-80(1)
	lfd 23,-72(1)
	lfd 24,-64(1)
	lfd 25,-56(1)
	lfd 26,-48(1)
	lfd 27,-40(1)
	lfd 28,-32(1)
	lfd 29,-24(1)
	lfd 30,-16(1)
	lfd 31,-8(1)
	blr
	.p2align 4,,15
.L10:
	li 5,15
	mr 10,30
	addi 9,30,128
	addi 8,31,192
	mr 12,7
	mtctr 5
	.p2align 4,,15
.L3:
	lfsu 0,-4(9)
	lfsu 14,4(6)
	lfsu 1,4(10)
	lfsu 22,-4(8)
	lfs 30,256(9)
	lfs 31,512(9)
	lfs 15,256(6)
	fmuls 0,14,0
	lfs 16,512(6)
	lfs 18,1024(6)
	lfs 19,1280(6)
	lfs 24,512(8)
	lfs 29,1792(8)
	lfs 17,768(6)
	lfs 20,1536(6)
	lfs 21,1792(6)
	fneg 0,0
	lfs 23,256(8)
	fmuls 1,14,1
	fmuls 30,15,30
	lfs 25,768(8)
	lfs 26,1024(8)
	lfs 27,1280(8)
	lfs 28,1536(8)
	fmuls 31,16,31
	lfs 13,768(9)
	lfs 2,1024(9)
	lfs 3,1280(9)
	lfs 4,1536(9)
	lfs 5,1792(9)
	lfs 6,128(9)
	lfs 7,384(9)
	lfs 8,640(9)
	lfs 9,896(9)
	lfs 10,1152(9)
	lfs 11,1408(9)
	lfs 12,1664(9)
	fsubs 0,0,30
	lfs 30,256(10)
	fmuls 13,17,13
	fmuls 2,18,2
	fmuls 3,19,3
	fmuls 4,20,4
	fmuls 5,21,5
	fmuls 6,22,6
	lfs 14,1920(9)
	fmuls 8,24,8
	fmuls 7,23,7
	fmuls 10,26,10
	fmuls 9,25,9
	fsubs 0,0,31
	lfs 31,768(10)
	fmuls 11,27,11
	fmuls 15,15,30
	lfs 30,512(10)
	fmuls 12,28,12
	fmuls 14,29,14
	fsubs 0,0,13
	lfs 13,1024(10)
	fmuls 17,17,31
	lfs 31,1280(10)
	fadds 1,1,15
	fmuls 16,16,30
	fsubs 2,0,2
	lfs 0,128(10)
	fmuls 13,18,13
	fmuls 18,19,31
	fadds 19,1,16
	lfs 1,1792(10)
	lfs 31,1536(10)
	fsubs 3,2,3
	lfs 2,896(10)
	fmuls 22,22,0
	lfs 0,1152(10)
	fadds 19,19,17
	fmuls 21,21,1
	lfs 1,640(10)
	fmuls 20,20,31
	lfs 31,384(10)
	fsubs 4,3,4
	lfs 3,1664(10)
	fmuls 25,25,2
	lfs 2,1408(10)
	fadds 19,19,13
	fmuls 26,26,0
	lfs 0,1920(10)
	fmuls 1,24,1
	fsubs 5,4,5
	fmuls 23,23,31
	fmuls 28,28,3
	fadds 19,19,18
	fmuls 27,27,2
	fmuls 0,29,0
	fsubs 6,5,6
	fadds 24,19,20
	fsubs 7,6,7
	fadds 24,24,21
	fsubs 8,7,8
	fsubs 24,24,22
	fsubs 9,8,9
	fsubs 24,24,23
	fsubs 10,9,10
	fsubs 24,24,1
	fsubs 11,10,11
	fsubs 29,24,25
	fsubs 12,11,12
	fsubs 29,29,26
	fsubs 14,12,14
	fsubs 29,29,27
	fsubs 29,29,28
	fsubs 29,29,0
	stfs 29,0(12)
	stfs 14,0(11)
	add 12,12,4
	add 11,11,3
	bdnz .L3
	b .L4
	.long 0
	.byte 0,0,2,1,146,5,0,0
	.size	ff_mpadsp_apply_window_float,.-ff_mpadsp_apply_window_float
	.ident	"GCC: (GNU) 10.3.0"
	.section	.note.GNU-stack,"",@progbits
	.file	"memcpy.c"
	.machine power9
	.abiversion 2
	.section	".text"
	.align 2
	.globl memcpy
	.type	memcpy, @function
memcpy:
	std 31,-8(1)
	stdu 1,-96(1)
	mr 31,1
	std 3,48(31)
	std 4,56(31)
	std 5,64(31)
	ld 9,48(31)
	std 9,32(31)
	ld 9,56(31)
	std 9,40(31)
	b .Lmemcpy_2
.Lmemcpy_3:
	ld 10,40(31)
	addi 9,10,1
	std 9,40(31)
	ld 9,32(31)
	addi 8,9,1
	std 8,32(31)
	lbz 10,0(10)
	stb 10,0(9)
.Lmemcpy_2:
	ld 9,64(31)
	addi 10,9,-1
	std 10,64(31)
	cmpdi 0,9,0
	bne 0,.Lmemcpy_3
	ld 9,48(31)
	mr 3,9
	addi 1,31,96
	ld 31,-8(1)
	blr
	.long 0
	.byte 0,0,0,0,128,1,0,1
	.size	memcpy,.-memcpy
	.ident	"GCC: (GNU) 10.3.0"
	.section	.note.GNU-stack,"",@progbits
