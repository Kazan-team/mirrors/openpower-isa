This directory contains OpenPOWER ISA related machine-readable files
The files in this directory have come from many locations:

* isatables CSV files are from Microwatt decode1.vhdl which is
  CC 4.0 Copyrighted
  https://github.com/antonblanchard/microwatt/blob/master/decode1.vhdl
* isatables fields.txt is from the OpenPOWER v3.0B specification which
  is managed by the OpenPOWER Foundation
* isa markdown files are extracted from the OpenPOWER v3.0B specification which
  is managed by the OpenPOWER Foundation
* python files are written by Libre-SOC team members and are LGPLv3+ licensed

