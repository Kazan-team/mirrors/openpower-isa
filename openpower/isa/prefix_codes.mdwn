<!-- https://libre-soc.org/openpower/prefix_codes/ -->

# [DRAFT] Prefix-code decode

VA-Form

* pcdec. RT,RA,RB,RC

Pseudo-code:

    tree[0:63] <- (RB)
    mode <- tree[62:63]
    tree[62:63] <- 0
    ra_used <- 0b0
    in_bits[0:63] <- (RC|0)
    if in_bits = 0 then
        in_bits[0:63] <- 1
    orig_in_bits <- in_bits
    tree_index <- 1
    found <- 0b0
    hit_end <- 0b0
    do bit_length = 1 to 6
        in_bit <- in_bits[63]
        if in_bits = 1 then
            if ra_used | (_RA = 0) then
                hit_end <- 0b1
                leave
            ra_used <- 0b1
            in_bit <- (RA)[63]
            in_bits <- 0b1 || (RA)[0:62]
        else
            in_bits <- 0b0 || in_bits[0:62]
        # walk the binary tree in `tree` from parent to the selected child
        tree_index <- tree_index * 2
        if in_bit = 1 then
            tree_index <- tree_index + 1
        if tree_index < 64 then
            if tree[63 - tree_index] then
                found <- 0b1
                leave
    compressed_index <- 0
    do i = 0 to 127
        possible <- 1
        j <- i
        do while j >= 4
            j <- j / 2
            if tree[63 - j] then
                possible <- 0
                leave
        if i = tree_index then
            leave
        else if i >= 64 then
            compressed_index <- compressed_index + possible
        else if tree[63 - i] = 1 then
            compressed_index <- compressed_index + possible
    switch(mode)
        case(0):
            RT[0:63] <- tree_index
            if ¬found then
                in_bits <- orig_in_bits
                ra_used <- 0b0
        case(1):
            RT[0:63] <- tree_index
            if hit_end then
                in_bits <- orig_in_bits
                ra_used <- 0b0
        case(2):
            RT[0:63] <- compressed_index
            if ¬found then
                in_bits <- orig_in_bits
                ra_used <- 0b0
                RT[0:63] <- tree_index
        default:
            RT[0:63] <- compressed_index
            if hit_end then
                in_bits <- orig_in_bits
                ra_used <- 0b0
    RS <- in_bits
    CR0 <- ra_used || (tree_index >= 64) || found || hit_end

Special Registers Altered:

    CR0
