<!-- TODO DRAFT Floating-point Shifted store instructions. -->

# Store Floating-Point Single Indexed Shifted

X-Form

* stfssx FRS,RA,RB,SH

Pseudo-code:

    EA <- (RA|0) + (RB)>>(SH+1)
    MEM(EA, 4)<- SINGLE( (FRS) )

Description:

    Let the effective address (EA) be the sum of (RA|0) with
    the contents of register RB shifted by (SH+1).

    The contents of register FRS are converted to single
    format (see page 142) and stored into the word in stor-
    age addressed by EA.

Special Registers Altered:

    None

# Store Floating-Point Single with Update Indexed Shifted

X-Form

* stfsusx FRS,RA,RB,SH

Pseudo-code:

    EA <- (RA) + (RB)>>(SH+1)
    MEM(EA, 4)<- SINGLE( (FRS) )
    RA <- EA

Description:

    Let the effective address (EA) be the sum of the contents of
    register RB shifted by (SH+1), and the contents of register RA.

    The contents of register FRS are converted to single
    format (see page 142) and stored into the word in stor-
    age addressed by EA.

    EA is placed into register RA.

    If RA=0, the instruction form is invalid.

Special Registers Altered:

    None

# Store Floating-Point Double Indexed Shifted

X-Form

* stfdsx FRS,RA,RB,SH

Pseudo-code:

    EA <- (RA|0) + (RB)>>(SH+1)
    MEM(EA, 8)<- (FRS) 

Description:

    Let the effective address (EA) be the sum (RA|0)+(RB).
    Let the effective address (EA) be the sum of (RA|0) with
    the contents of register RB shifted by (SH+1).

    The contents of register FRS are stored into the dou-
    bleword in storage addressed by EA.

Special Registers Altered:

    None

# Store Floating-Point Double with Update Indexed Shifted

X-Form

* stfdusx FRS,RA,RB,SH

Pseudo-code:

    EA <- (RA) + (RB)>>(SH+1)
    MEM(EA, 8)<- (FRS) 
    RA <- EA

Description:

    Let the effective address (EA) be the sum of the contents of
    register RB shifted by (SH+1), and the contents of register RA.

    The contents of register FRS are stored into the dou-
    bleword in storage addressed by EA.

    EA is placed into register RA.

    If RA=0, the instruction form is invalid.

Special Registers Altered:

    None

# Store Floating-Point as Integer Word Indexed Shifted

X-Form

* stfiwsx FRS,RA,RB,SH

Pseudo-code:

    EA <- (RA|0) + (RB)>>(SH+1)
    MEM(EA, 8)<- (FRS)[32:63]

Description:

    Let the effective address (EA) be the sum of (RA|0) with
    the contents of register RB shifted by (SH+1).

    (FRS)[32:63] are stored, without conversion, into the word
    in storage addressed by EA.

    If the contents of register FRS were produced, either
    directly or indirectly, by a Load Floating-Point Single
    instruction, a single-precision Arithmetic instruction, or
    frsp, then the value stored is undefined. (The contents
    of register FRS are produced directly by such an
    instruction if FRS is the target register for the instruc-
    tion. The contents of register FRS are produced indi-
    rectly by such an instruction if FRS is the final target
    register of a sequence of one or more Floating-Point
    Move instructions, with the input to the sequence hav-
    ing been produced directly by such an instruction.)

Special Registers Altered:

    None
