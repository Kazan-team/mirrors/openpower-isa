<!-- This defines instructions described in PowerISA Version 3.0 B Book 1 -->

<!-- This defines instructions that store from a register to RAM -->

<!-- Note that these pages also define equivalent load instructions, -->
<!-- these are described in fixedload.mdwn -->

<!-- Section 3.3.3 Fixed-Point Store Instructions pages 54 - 57 -->
<!-- Section 3.3.3.1 64-bit Fixed-Point Store Instructions page 57 -->
<!-- Section 3.3.4 Fixed Point Load and Store Quadword Instructions pages 58 - 59 -->
<!-- Section 3.3.5 Fixed-Point Load and Store with Byte Reversal Instructions page 60 -->
<!-- Section 3.3.5.1 64-Bit Load and Store with Byte Reversal Instructions page 61 -->
<!-- Section 3.3.6 Fixed-Point Load and Store Multiple Instructions page 62 -->

<!-- Section 3.3.3 Fixed-Point Store Instructions pages 54 - 57 -->

# Store Byte Shifted Indexed

X-Form

* stbsx RS,RA,RB,SH

Pseudo-code:

    b <- (RA|0)
    EA <- b + (RB) << (SH+1)
    MEM(EA, 1) <- (RS)[XLEN-8:XLEN-1]

Description:

    Let the effective address (EA) be the sum of the contents of
    register RB shifted by (SH+1), and (RA|0).

    RS [56:63] are stored into the byte in storage addressed by EA.

Special Registers Altered:

    None

# Store Byte Shifted with Update Indexed

X-Form

* stbsux RS,RA,RB,SH

Pseudo-code:

    EA <- (RA) + (RB) << (SH+1)
    MEM(EA, 1) <- (RS)[XLEN-8:XLEN-1]
    RA <- EA

Description:

    Let the effective address (EA) be the sum of the contents of
    register RB shifted by (SH+1), and (RA).

    RS[56:63] are stored into the byte in storage addressed by EA.

    EA is placed into register RA.

    If RA=0, the instruction form is invalid.

Special Registers Altered:

    None

# Store Halfword Shifted Indexed

X-Form

* sthsx RS,RA,RB,SH

Pseudo-code:

    b <- (RA|0)
    EA <- b + (RB) << (SH+1)
    MEM(EA, 2) <- (RS)[XLEN-16:XLEN-1]

Description:

    Let the effective address (EA) be the sum of the contents of
    register RB shifted by (SH+1), and (RA|0).

    RS[48:63] are stored into the halfword in storage addressed by EA.

Special Registers Altered:

    None

# Store Halfword Shifted with Update Indexed

X-Form

* sthsux RS,RA,RB,SH

Pseudo-code:

    EA <- (RA) + (RB) << (SH+1)
    MEM(EA, 2) <- (RS)[XLEN-16:XLEN-1]
    RA <- EA

Description:

    Let the effective address (EA) be the sum of the contents of
    register RB shifted by (SH+1), and (RA).

    RS[48:63] are stored into the halfword in storage addressed by EA.

    EA is placed into register RA.

    If RA=0, the instruction form is invalid.

Special Registers Altered:

    None

# Store Word Shifted Indexed

X-Form

* stwsx RS,RA,RB,SH

Pseudo-code:

    b <- (RA|0)
    EA <- b + (RB) << (SH+1)
    MEM(EA, 4) <- (RS)[XLEN-32:XLEN-1]

Description:

    Let the effective address (EA) be the sum of the contents of
    register RB shifted by (SH+1), and (RA|0).

    RS[32:63] are stored into the word in storage addressed by EA.

Special Registers Altered:

    None

# Store Word Shifted with Update Indexed

X-Form

* stwsux RS,RA,RB,SH

Pseudo-code:

    EA <- (RA) + (RB) << (SH+1)
    MEM(EA, 4) <- (RS)[XLEN-32:XLEN-1]
    RA <- EA

Description:

    Let the effective address (EA) be the sum of the contents of
    register RB shifted by (SH+1), and (RA).

    RS[32:63] are stored into the word in storage addressed by EA.

    EA is placed into register RA.

    If RA=0, the instruction form is invalid.

Special Registers Altered:

    None



<!-- Section 3.3.3.1 64-bit Fixed-Point Store Instructions page 57 -->

# Store Doubleword Shifted Indexed

X-Form

* stdsx RS,RA,RB,SH

Pseudo-code:

    b <- (RA|0)
    EA <- b + (RB) << (SH+1)
    MEM(EA, 8) <- (RS)

Description:

    Let the effective address (EA) be the sum of the contents of
    register RB shifted by (SH+1), and (RA|0).

    (RS) is stored into the doubleword in storage addressed by EA.

Special Registers Altered:

    None

# Store Doubleword Shifted with Update Indexed

X-Form

* stdsux RS,RA,RB,SH

Pseudo-code:

    EA <- (RA) + (RB) << (SH+1)
    MEM(EA, 8) <- (RS)
    RA <- EA

Description:

    Let the effective address (EA) be the sum of the contents of
    register (RB) shifted by (SH+1), and (RA).

    (RS) is stored into the doubleword in storage addressed by EA.

    EA is placed into register RA.

    If RA=0, the instruction form is invalid.

Special Registers Altered:

    None


<!-- Section 3.3.5 Fixed-Point Load and Store with Byte Reversal Instructions page 60 -->

# Store Halfword Byte-Reverse Shifted Indexed

X-Form

* sthbrsx RS,RA,RB,SH

Pseudo-code:

    b <- (RA|0)
    EA <- b + (RB) << (SH+1)
    MEM(EA, 2) <- (RS) [56:63] || (RS)[48:55]

Description:

    Let the effective address (EA) be the sum of the contents of
    register RB shifted by (SH+1), and (RA|0).

    (RS)56:63 are stored into bits 0:7 of the halfword in storage
    addressed by EA.  (RS)[48:55] are stored into bits 8:15 of
    the halfword in storage addressed by EA.

Special Registers Altered:

    None

# Store Word Byte-Reverse Shifted Indexed

X-Form

* stwbrsx RS,RA,RB,SH

Pseudo-code:

    b <- (RA|0)
    EA <- b + (RB) << (SH+1)
    MEM(EA, 4) <- ((RS)[56:63] || (RS)[48:55] || (RS)[40:47]
                   ||(RS)[32:39])

Description:

    Let the effective address (EA) be the sum of the contents of
    register RB shifted by (SH+1), and (RA|0).

    (RS)[56:63] are stored into bits 0:7 of the
    word in storage addressed by EA. (RS) [48:55] are stored
    into bits 8:15 of the word in storage addressed by EA.
    (RS)[40:47] are stored into bits 16:23 of the word in stor-
    age addressed by EA. (RS) [32:39] are stored into bits
    24:31 of the word in storage addressed by EA.

Special Registers Altered:

    None

<!-- Section 3.3.5.1 64-Bit Load and Store with Byte Reversal Instructions page 61 -->

# Store Doubleword Byte-Reverse Shifted Indexed

X-Form

* stdbrsx RS,RA,RB,SH

Pseudo-code:

    b <- (RA|0)
    EA <- b + (RB) << (SH+1)
    MEM(EA, 8) <- ((RS) [56:63] || (RS)[48:55]
                    || (RS)[40:47] || (RS)[32:39]
                    || (RS)[24:31] || (RS)[16:23]
                    || (RS)[8:15]  || (RS)[0:7])

Description:

    Let the effective address (EA) be the sum of the contents of
    register RB shifted by (SH+1), and (RA|0).

    (RS)[56:63] are stored into bits 0:7 of the
    doubleword in storage addressed by EA. (RS)[48:55] are
    stored into bits 8:15 of the doubleword in storage
    addressed by EA. (RS) [40:47] are stored into bits 16:23 of
    the doubleword in storage addressed by EA. (RS)[32:39]
    are stored into bits 23:31 of the doubleword in storage
    addressed by EA. (RS) [24:31] are stored into bits 32:39 of
    the doubleword in storage addressed by EA. (RS)[16:23]
    are stored into bits 40:47 of the doubleword in storage
    addressed by EA. (RS)[8:15] are stored into bits 48:55 of
    the doubleword in storage addressed by EA. (RS)[0:7]
    are stored into bits 56:63 of the doubleword in storage
    addressed by EA.

Special Registers Altered:

    None

<!-- Checked March 2021 -->
