# Qemu test directory

To launch and debug qemu, run the following:
```
make
./launch.sh
powerpc64-linux-gnu-gdb -x gdbscript
```
