# SPDX-License-Identifier: LGPLv3+
# Copyright (C) 2020, 2021 Luke Kenneth Casson Leighton <lkcl@lkcl.net>
# Copyright (C) 2021 Tobias Platen
# Funded by NLnet http://nlnet.nl
"""core of the python-based POWER9 simulator

this is part of a cycle-accurate POWER9 simulator.  its primary purpose is
not speed, it is for both learning and educational purposes, as well as
a method of verifying the HDL.

related bugs:

* https://bugs.libre-soc.org/show_bug.cgi?id=604
"""

#from nmigen.sim import Settle
from copy import copy
from openpower.decoder.selectable_int import (FieldSelectableInt, SelectableInt,
                                        selectconcat)
from openpower.decoder.helpers import exts, gtu, ltu, undefined
from openpower.decoder.isa.mem import Mem, MemException
from openpower.consts import MSRb  # big-endian (PowerISA versions)
from openpower.util import log

import math
import sys
import unittest

# very quick, TODO move to SelectableInt utils later
def genmask(shift, size):
    res = SelectableInt(0, size)
    for i in range(size):
        if i < shift:
            res[size-1-i] = SelectableInt(1, 1)
    return res

# NOTE: POWER 3.0B annotation order!  see p4 1.3.2
# MSB is indexed **LOWEST** (sigh)
# from gem5 radixwalk.hh
# Bitfield<63> valid;  64 - (63 + 1) = 0
# Bitfield<62> leaf;   64 - (62 + 1) = 1

def rpte_valid(r):
    return bool(r[0])

def rpte_leaf(r):
    return bool(r[1])

## Shift address bits 61--12 right by 0--47 bits and
## supply the least significant 16 bits of the result.
def addrshift(addr,shift):
    log("addrshift")
    log(addr)
    log(shift)
    x = addr.value >> shift.value
    return SelectableInt(x, 16)

def RTS2(data):
    return data[56:59]

def RTS1(data):
    return data[1:3]

def RTS(data):
    zero = SelectableInt(0, 1)
    return selectconcat(zero, RTS2(data), RTS1(data))

def NLB(x):
    """
    Next Level Base
    right shifted by 8
    """
    return x[4:56] # python numbering end+1

def NLS(x):
    """
    Next Level Size (PATS and RPDS in same bits btw)
    NLS >= 5
    """
    return x[59:64] # python numbering end+1

def RPDB(x):
    """
    Root Page Directory Base
    power isa docs says 4:55 investigate
    """
    return x[8:56] # python numbering end+1

"""
    Get Root Page

    //Accessing 2nd double word of partition table (pate1)
    //Ref: Power ISA Manual v3.0B, Book-III, section 5.7.6.1
    //           PTCR Layout
    // ====================================================
    // -----------------------------------------------
    // | /// |     PATB                | /// | PATS  |
    // -----------------------------------------------
    // 0     4                       51 52 58 59    63
    // PATB[4:51] holds the base address of the Partition Table,
    // right shifted by 12 bits.
    // This is because the address of the Partition base is
    // 4k aligned. Hence, the lower 12bits, which are always
    // 0 are ommitted from the PTCR.
    //
    // Thus, The Partition Table Base is obtained by (PATB << 12)
    //
    // PATS represents the partition table size right-shifted by 12 bits.
    // The minimal size of the partition table is 4k.
    // Thus partition table size = (1 << PATS + 12).
    //
    //        Partition Table
    //  ====================================================
    //  0    PATE0            63  PATE1             127
    //  |----------------------|----------------------|
    //  |                      |                      |
    //  |----------------------|----------------------|
    //  |                      |                      |
    //  |----------------------|----------------------|
    //  |                      |                      | <-- effLPID
    //  |----------------------|----------------------|
    //           .
    //           .
    //           .
    //  |----------------------|----------------------|
    //  |                      |                      |
    //  |----------------------|----------------------|
    //
    // The effective LPID  forms the index into the Partition Table.
    //
    // Each entry in the partition table contains 2 double words, PATE0, PATE1,
    // corresponding to that partition.
    //
    // In case of Radix, The structure of PATE0 and PATE1 is as follows.
    //
    //     PATE0 Layout
    // -----------------------------------------------
    // |1|RTS1|/|     RPDB          | RTS2 |  RPDS   |
    // -----------------------------------------------
    //  0 1  2 3 4                55 56  58 59      63
    //
    // HR[0] : For Radix Page table, first bit should be 1.
    // RTS1[1:2] : Gives one fragment of the Radix treesize
    // RTS2[56:58] : Gives the second fragment of the Radix Tree size.
    // RTS = (RTS1 << 3 + RTS2) + 31.
    //
    // RPDB[4:55] = Root Page Directory Base.
    // RPDS = Logarithm of Root Page Directory Size right shifted by 3.
    //        Thus, Root page directory size = 1 << (RPDS + 3).
    //        Note: RPDS >= 5.
    //
    //   PATE1 Layout
    // -----------------------------------------------
    // |///|       PRTB             |  //  |  PRTS   |
    // -----------------------------------------------
    // 0  3 4                     51 52  58 59     63
    //
    // PRTB[4:51]   = Process Table Base. This is aligned to size.
    // PRTS[59: 63] = Process Table Size right shifted by 12.
    //                Minimal size of the process table is 4k.
    //                Process Table Size = (1 << PRTS + 12).
    //                Note: PRTS <= 24.
    //
    //                Computing the size aligned Process Table Base:
    //                   table_base = (PRTB  & ~((1 << PRTS) - 1)) << 12
    //                Thus, the lower 12+PRTS bits of table_base will
    //                be zero.


    //Ref: Power ISA Manual v3.0B, Book-III, section 5.7.6.2
    //
    //        Process Table
    // ==========================
    //  0    PRTE0            63  PRTE1             127
    //  |----------------------|----------------------|
    //  |                      |                      |
    //  |----------------------|----------------------|
    //  |                      |                      |
    //  |----------------------|----------------------|
    //  |                      |                      | <-- effPID
    //  |----------------------|----------------------|
    //           .
    //           .
    //           .
    //  |----------------------|----------------------|
    //  |                      |                      |
    //  |----------------------|----------------------|
    //
    // The effective Process id (PID) forms the index into the Process Table.
    //
    // Each entry in the partition table contains 2 double words, PRTE0, PRTE1,
    // corresponding to that process
    //
    // In case of Radix, The structure of PRTE0 and PRTE1 is as follows.
    //
    //     PRTE0 Layout
    // -----------------------------------------------
    // |/|RTS1|/|     RPDB          | RTS2 |  RPDS   |
    // -----------------------------------------------
    //  0 1  2 3 4                55 56  58 59      63
    //
    // RTS1[1:2] : Gives one fragment of the Radix treesize
    // RTS2[56:58] : Gives the second fragment of the Radix Tree size.
    // RTS = (RTS1 << 3 + RTS2) << 31,
    //        since minimal Radix Tree size is 4G.
    //
    // RPDB = Root Page Directory Base.
    // RPDS = Root Page Directory Size right shifted by 3.
    //        Thus, Root page directory size = RPDS << 3.
    //        Note: RPDS >= 5.
    //
    //   PRTE1 Layout
    // -----------------------------------------------
    // |                      ///                    |
    // -----------------------------------------------
    // 0                                            63
    // All bits are reserved.


"""

testmem = {

           0x10000:    # PARTITION_TABLE_2 (not implemented yet)
                       # PATB_GR=1 PRTB=0x1000 PRTS=0xb
           0x800000000100000b,

           0x30000:     # RADIX_ROOT_PTE
                        # V = 1 L = 0 NLB = 0x400 NLS = 9
           0x8000000000040009,
           0x40000:     # RADIX_SECOND_LEVEL
                        # 	   V = 1 L = 1 SW = 0 RPN = 0
	                    # R = 1 C = 1 ATT = 0 EAA 0x7
           0xc000000000000187,

           0x1000000:   # PROCESS_TABLE_3
                       # RTS1 = 0x2 RPDB = 0x300 RTS2 = 0x5 RPDS = 13
           0x40000000000300ad,
          }

# this one has a 2nd level RADIX with a RPN of 0x5000
testmem2 = {

           0x10000:    # PARTITION_TABLE_2 (not implemented yet)
                       # PATB_GR=1 PRTB=0x1000 PRTS=0xb
           0x800000000100000b,

           0x30000:     # RADIX_ROOT_PTE
                        # V = 1 L = 0 NLB = 0x400 NLS = 9
           0x8000000000040009,
           0x40000:     # RADIX_SECOND_LEVEL
                        # 	   V = 1 L = 1 SW = 0 RPN = 0x5000
	                    # R = 1 C = 1 ATT = 0 EAA 0x7
           0xc000000005000187,

           0x1000000:   # PROCESS_TABLE_3
                       # RTS1 = 0x2 RPDB = 0x300 RTS2 = 0x5 RPDS = 13
           0x40000000000300ad,
          }

testresult = """
    prtbl = 1000000
    DCACHE GET 1000000 PROCESS_TABLE_3
    DCACHE GET 30000 RADIX_ROOT_PTE V = 1 L = 0
    DCACHE GET 40000 RADIX_SECOND_LEVEL V = 1 L = 1
    DCACHE GET 10000 PARTITION_TABLE_2
translated done 1 err 0 badtree 0 addr 40000 pte 0
"""

# see qemu/target/ppc/mmu-radix64.c for reference
class RADIX:
    def __init__(self, mem, caller):
        self.mem = mem
        self.caller = caller
        if caller is not None:
            log("caller")
            log(caller)
            self.dsisr = self.caller.spr["DSISR"]
            self.dar   = self.caller.spr["DAR"]
            self.pidr  = self.caller.spr["PIDR"]
            self.prtbl = self.caller.spr["PRTBL"]
            self.msr   = self.caller.msr

        # cached page table stuff
        self.pgtbl0 = 0
        self.pt0_valid = False
        self.pgtbl3 = 0
        self.pt3_valid = False

    def __call__(self, addr, sz):
        val = self.ld(addr.value, sz, swap=False)
        log("RADIX memread", addr, sz, val)
        return SelectableInt(val, sz*8)

    def ld(self, address, width=8, swap=True, check_in_mem=False,
                 instr_fetch=False):
        if instr_fetch:
            mode = 'EXECUTE'
        else:
            mode = 'LOAD'
        priv = ~(self.msr[MSRb.PR].value) # problem-state ==> privileged
        virt = (self.msr[MSRb.DR].value)  # DR -> virtual
        log("RADIX: ld from addr 0x%x width %d mode %s "
               "priv %d virt %d" % (address, width, mode, priv, virt))

        # virtual mode does a lookup to new address, otherwise use real addr
        addr = SelectableInt(address, 64)
        if virt:
            addr = self._walk_tree(addr, mode, priv)
        addr = addr.value

        # use address to load from phys address
        data = self.mem.ld(addr, width, swap, check_in_mem)
        self.last_ld_addr = self.mem.last_ld_addr

        # XXX set SPRs on error
        return data

    # TODO implement
    def st(self, address, v, width=8, swap=True):

        priv = ~(self.msr[MSRb.PR].value) # problem-state ==> privileged
        virt = (self.msr[MSRb.DR].value)  # DR -> virtual
        log("RADIX: st to addr 0x%x width %d data %x "
              "priv %d virt %d " % (address, width, v, priv, virt))
        mode = 'STORE'

        # virtual mode does a lookup to new address, otherwise use real addr
        addr = SelectableInt(address, 64)
        if virt:
            addr = self._walk_tree(addr, mode, priv)
        addr = addr.value

        # use address to store at phys address
        res = self.mem.st(addr, v, width, swap)
        self.last_st_addr = self.mem.last_st_addr

        # XXX set SPRs on error
        return res

    def memassign(self, addr, sz, val):
        log("memassign", addr, sz, val)
        self.st(addr.value, val.value, sz, swap=False)

    def _next_level(self, addr, check_in_mem):
        # implement read access to mmu mem here

        # DO NOT perform byte-swapping: load 8 bytes (that's the entry size)
        value = self.mem.ld(addr.value, 8, False, check_in_mem)
        if value is None:
            return "address lookup %x not found" % addr.value
        # assert(value is not None, "address lookup %x not found" % addr.value)

        data = SelectableInt(value, 64) # convert to SelectableInt
        log("addr", hex(addr.value))
        log("value", hex(value))
        return data;

    def _walk_tree(self, addr, mode, priv=1):
        """walk tree

        // vaddr                    64 Bit
        // vaddr |-----------------------------------------------------|
        //       | Unused    |  Used                                   |
        //       |-----------|-----------------------------------------|
        //       | 0000000   | usefulBits = X bits (typically 52)      |
        //       |-----------|-----------------------------------------|
        //       |           |<--Cursize---->|                         |
        //       |           |    Index      |                         |
        //       |           |    into Page  |                         |
        //       |           |    Directory  |                         |
        //       |-----------------------------------------------------|
        //                        |                       |
        //                        V                       |
        // PDE  |---------------------------|             |
        //      |V|L|//|  NLB       |///|NLS|             |
        //      |---------------------------|             |
        // PDE = Page Directory Entry                     |
        // [0] = V = Valid Bit                            |
        // [1] = L = Leaf bit. If 0, then                 |
        // [4:55] = NLB = Next Level Base                 |
        //                right shifted by 8              |
        // [59:63] = NLS = Next Level Size                |
        //            |    NLS >= 5                       |
        //            |                                   V
        //            |                     |--------------------------|
        //            |                     |   usfulBits = X-Cursize  |
        //            |                     |--------------------------|
        //            |---------------------><--NLS-->|                |
        //                                  | Index   |                |
        //                                  | into    |                |
        //                                  | PDE     |                |
        //                                  |--------------------------|
        //                                                    |
        // If the next PDE obtained by                        |
        // (NLB << 8 + 8 * index) is a                        |
        // nonleaf, then repeat the above.                    |
        //                                                    |
        // If the next PDE is a leaf,                         |
        // then Leaf PDE structure is as                      |
        // follows                                            |
        //                                                    |
        //                                                    |
        // Leaf PDE                                           |
        // |------------------------------|           |----------------|
        // |V|L|sw|//|RPN|sw|R|C|/|ATT|EAA|           | usefulBits     |
        // |------------------------------|           |----------------|
        // [0] = V = Valid Bit                                 |
        // [1] = L = Leaf Bit = 1 if leaf                      |
        //                      PDE                            |
        // [2] = Sw = Sw bit 0.                                |
        // [7:51] = RPN = Real Page Number,                    V
        //          real_page = RPN << 12 ------------->  Logical OR
        // [52:54] = Sw Bits 1:3                               |
        // [55] = R = Reference                                |
        // [56] = C = Change                                   V
        // [58:59] = Att =                                Physical Address
        //           0b00 = Normal Memory
        //           0b01 = SAO
        //           0b10 = Non Idenmpotent
        //           0b11 = Tolerant I/O
        // [60:63] = Encoded Access
        //           Authority
        //
        """
        # get sprs
        log("_walk_tree")
        pidr  = self.caller.spr["PIDR"]
        prtbl = self.caller.spr["PRTBL"]
        log("PIDR", pidr)
        log("PRTBL", prtbl)
        p = addr[55:63]
        log("last 8 bits ----------")
        log()

        # get address of root entry
        # need to fetch process table entry
        # v.shift := unsigned('0' & r.prtbl(4 downto 0));
        shift = selectconcat(SelectableInt(0, 1), NLS(prtbl))
        addr_next = self._get_prtable_addr(shift, prtbl, addr, pidr)
        log("starting with prtable, addr_next", addr_next)

        assert(addr_next.bits == 64)
        #only for first unit tests assert(addr_next.value == 0x1000000)

        # read an entry from prtable, decode PTRE
        data = self._next_level(addr_next, check_in_mem=False)
        log("pr_table", data)
        pgtbl = data # this is cached in microwatt (as v.pgtbl3 / v.pgtbl0)
        (rts, mbits, pgbase) = self._decode_prte(pgtbl)
        log("pgbase", pgbase)

        # WIP
        if mbits == 0:
            exc = MemException("invalid")
            exc.mode = mode
            raise exc

        # mask_size := mbits(4 downto 0);
        mask_size = mbits[0:5]
        assert(mask_size.bits == 5)
        log("before segment check ==========")
        log("mask_size:", bin(mask_size.value))
        log("mbits:", bin(mbits.value))

        log("calling segment_check")

        shift = self._segment_check(addr, mask_size, shift)
        log("shift", shift)

        if isinstance(addr, str):
            return addr
        if isinstance(shift, str):
            return shift

        old_shift = shift

        mask = mask_size

        # walk tree
        while True:
            addrsh = addrshift(addr, shift)
            log("addrsh",addrsh)

            log("calling _get_pgtable_addr")
            log(mask)    #SelectableInt(value=0x9, bits=4)
            log(pgbase)  #SelectableInt(value=0x40000, bits=56)
            log(shift)   #SelectableInt(value=0x4, bits=16) #FIXME
            addr_next = self._get_pgtable_addr(mask, pgbase, addrsh)
            log("DONE addr_next", addr_next)

            log("nextlevel----------------------------")
            # read an entry
            data = self._next_level(addr_next, check_in_mem=False)
            valid = rpte_valid(data)
            leaf = rpte_leaf(data)

            log("    valid, leaf", valid, leaf)
            if not valid:
                exc = MemException("invalid")
                exc.mode = mode
                raise exc
            if leaf:
                log ("is leaf, checking perms")
                ok = self._check_perms(data, priv, mode)
                if ok == True: # data was ok, found phys address, return it?
                    paddr = self._get_pte(addrsh, addr, data)
                    log ("    phys addr", hex(paddr.value))
                    return paddr
                return ok # return the error code
            else:
                newlookup = self._new_lookup(data, shift, old_shift)
                if isinstance(newlookup, str):
                    return newlookup
                old_shift = shift # store old_shift before updating shift
                shift, mask, pgbase = newlookup
                log ("   next level", shift, mask, pgbase)

    def _get_pgbase(self, data):
        """
        v.pgbase := data(55 downto 8) & x"00"; NLB?
        """
        zero8 = SelectableInt(0, 8)
        ret = selectconcat(data[8:56], zero8)
        assert(ret.bits==56)
        return ret

    def _new_lookup(self, data, shift, old_shift):
        """
        mbits := unsigned('0' & data(4 downto 0));
        if mbits < 5 or mbits > 16 or mbits > r.shift then
            v.state := RADIX_FINISH;
            v.badtree := '1'; -- throw error
        else
            v.shift := v.shift - mbits;
            v.mask_size := mbits(4 downto 0);
            v.pgbase := data(55 downto 8) & x"00"; NLB?
            v.state := RADIX_LOOKUP; --> next level
        end if;
        """
        mbits = selectconcat(SelectableInt(0, 1), NLS(data))
        log("mbits=", mbits)
        if mbits < 5 or mbits > 16 or mbits > old_shift:
            log("badtree")
            return "badtree"
        # reduce shift (has to be done at same bitwidth)
        shift = shift - mbits
        assert mbits.bits == 6
        mask_size = mbits[2:6] # get 4 LSBs from 6-bit (using MSB0 numbering)
        pgbase = self._get_pgbase(data)
        return shift, mask_size, pgbase

    def _decode_prte(self, data):
        """PRTE0 Layout
           -----------------------------------------------
           |/|RTS1|/|     RPDB          | RTS2 |  RPDS   |
           -----------------------------------------------
            0 1  2 3 4                55 56  58 59      63
        """
        # note that SelectableInt does big-endian!  so the indices
        # below *directly* match the spec, unlike microwatt which
        # has to turn them around (to LE)
        rts, mbits = self._get_rts_nls(data)
        pgbase = self._get_pgbase(data)

        return (rts, mbits, pgbase)

    def _get_rts_nls(self, data):
        # rts = shift = unsigned('0' & data(62 downto 61) & data(7 downto 5));
        #                                        RTS1       RTS2
        rts = RTS(data)
        assert(rts.bits == 6) # variable rts : unsigned(5 downto 0);
        log("shift", rts)

        # mbits := unsigned('0' & data(4 downto 0));
        mbits = selectconcat(SelectableInt(0, 1), NLS(data))
        assert(mbits.bits == 6) #variable mbits : unsigned(5 downto 0);

        return rts, mbits

    def _segment_check(self, addr, mask_size, shift):
        """checks segment valid
            mbits := '0' & r.mask_size;
            v.shift := r.shift + (31 - 12) - mbits;
            nonzero := or(r.addr(61 downto 31) and not finalmask(30 downto 0));
            if r.addr(63) /= r.addr(62) or nonzero = '1' then
                v.state := RADIX_FINISH;
                v.segerror := '1';
            elsif mbits < 5 or mbits > 16 or mbits > (r.shift + (31 - 12)) then
                v.state := RADIX_FINISH;
                v.badtree := '1';
            else
                v.state := RADIX_LOOKUP;
        """
        # note that SelectableInt does big-endian!  so the indices
        # below *directly* match the spec, unlike microwatt which
        # has to turn them around (to LE)
        mbits = selectconcat(SelectableInt(0,1), mask_size)
        mask = genmask(shift, 44)
        nonzero = addr[2:33] & mask[13:44] # mask 31 LSBs (BE numbered 13:44)
        log ("RADIX _segment_check nonzero", bin(nonzero.value))
        log ("RADIX _segment_check addr[0-1]", addr[0].value, addr[1].value)
        if addr[0] != addr[1] or nonzero != 0:
            return "segerror"
        limit = shift + (31 - 12)
        if mbits.value < 5 or mbits.value > 16 or mbits.value > limit.value:
            return "badtree"
        new_shift = SelectableInt(limit.value - mbits.value, shift.bits)
        # TODO verify that returned result is correct
        return new_shift

    def _check_perms(self, data, priv, mode):
        """check page permissions
        // Leaf PDE                                           |
        // |------------------------------|           |----------------|
        // |V|L|sw|//|RPN|sw|R|C|/|ATT|EAA|           | usefulBits     |
        // |------------------------------|           |----------------|
        // [0] = V = Valid Bit                                 |
        // [1] = L = Leaf Bit = 1 if leaf                      |
        //                      PDE                            |
        // [2] = Sw = Sw bit 0.                                |
        // [7:51] = RPN = Real Page Number,                    V
        //          real_page = RPN << 12 ------------->  Logical OR
        // [52:54] = Sw Bits 1:3                               |
        // [55] = R = Reference                                |
        // [56] = C = Change                                   V
        // [58:59] = Att =                                Physical Address
        //           0b00 = Normal Memory
        //           0b01 = SAO
        //           0b10 = Non Idenmpotent
        //           0b11 = Tolerant I/O
        // [60:63] = Encoded Access
        //           Authority
        //
                    -- test leaf bit
                        -- check permissions and RC bits
                        perm_ok := '0';
                        if r.priv = '1' or data(3) = '0' then
                            if r.iside = '0' then
                                perm_ok := data(1) or (data(2) and not r.store);
                            else
                                -- no IAMR, so no KUEP support for now
                                -- deny execute permission if cache inhibited
                                perm_ok := data(0) and not data(5);
                            end if;
                        end if;
                        rc_ok := data(8) and (data(7) or not r.store);
                        if perm_ok = '1' and rc_ok = '1' then
                            v.state := RADIX_LOAD_TLB;
                        else
                            v.state := RADIX_FINISH;
                            v.perm_err := not perm_ok;
                            -- permission error takes precedence over RC error
                            v.rc_error := perm_ok;
                        end if;
        """
        # decode mode into something that matches microwatt equivalent code
        instr_fetch, store = 0, 0
        if mode == 'STORE':
            store = 1
        if mode == 'EXECUTE':
            inst_fetch = 1

        # check permissions and RC bits
        perm_ok = 0
        if priv == 1 or data[60] == 0:
            if instr_fetch == 0:
                perm_ok = data[62] | (data[61] & (store == 0))
            # no IAMR, so no KUEP support for now
            # deny execute permission if cache inhibited
            perm_ok = data[63] & ~data[58]
        rc_ok = data[55] & (data[56] | (store == 0))
        if perm_ok == 1 and rc_ok == 1:
            return True

        return "perm_err" if perm_ok == 0 else "rc_err"

    def _get_prtable_addr(self, shift, prtbl, addr, pid):
        """
        if r.addr(63) = '1' then
            effpid := x"00000000";
        else
            effpid := r.pid;
        end if;
        x"00" & r.prtbl(55 downto 36) &
                ((r.prtbl(35 downto 12) and not finalmask(23 downto 0)) or
                (effpid(31 downto 8) and finalmask(23 downto 0))) &
                effpid(7 downto 0) & "0000";
        """
        finalmask = genmask(shift, 44)
        finalmask24 = finalmask[20:44]
        log ("_get_prtable_addr", shift, prtbl, addr, pid,
                bin(finalmask24.value))
        if addr[0].value == 1:
            effpid = SelectableInt(0, 32)
        else:
            effpid = pid #self.pid # TODO, check on this
        zero8 = SelectableInt(0, 8)
        zero4 = SelectableInt(0, 4)
        res = selectconcat(zero8,
                           prtbl[8:28],                        #
                           (prtbl[28:52] & ~finalmask24) |     #
                           (effpid[0:24] & finalmask24),       #
                           effpid[24:32],
                           zero4
                           )
        return res

    def _get_pgtable_addr(self, mask_size, pgbase, addrsh):
        """
        x"00" & r.pgbase(55 downto 19) &
        ((r.pgbase(18 downto 3) and not mask) or (addrsh and mask)) &
        "000";
        """
        log("pgbase",pgbase)
        assert(pgbase.bits==56)
        mask16 = genmask(mask_size+5, 16)
        zero8 = SelectableInt(0, 8)
        zero3 = SelectableInt(0, 3)
        res = selectconcat(zero8,
                           pgbase[0:37],
                           (pgbase[37:53] & ~mask16) |
                           (addrsh       & mask16),
                           zero3
                           )
        return res

    def _get_pte(self, shift, addr, pde):
        """
        x"00" &
        ((r.pde(55 downto 12) and not finalmask) or
         (r.addr(55 downto 12) and finalmask))
        & r.pde(11 downto 0);
        """
        shift.value = 12
        finalmask = genmask(shift, 44)
        zero8 = SelectableInt(0, 8)
        rpn = pde[8:52]       # RPN = Real Page Number
        abits = addr[8:52] # non-masked address bits
        log("     get_pte RPN", hex(rpn.value))
        log("             abits", hex(abits.value))
        log("             shift", shift.value)
        log("             finalmask", bin(finalmask.value))
        res = selectconcat(zero8,
                           (rpn  & ~finalmask) | #
                           (abits & finalmask),   #
                           addr[52:64],
                           )
        return res


class TestRadixMMU(unittest.TestCase):

    def test_genmask(self):
        shift = SelectableInt(5, 6)
        mask = genmask(shift, 43)
        log ("    mask", bin(mask.value))

        self.assertEqual(mask.value, 0b11111, "mask should be 5 1s")

    def test_RPDB(self):
        inp = SelectableInt(0x40000000000300ad, 64)

        rtdb = RPDB(inp)
        log("rtdb",rtdb,bin(rtdb.value))
        self.assertEqual(rtdb.value,0x300,"rtdb should be 0x300")

        result = selectconcat(rtdb,SelectableInt(0,8))
        log("result",result)

    def test_get_pgtable_addr(self):

        mem = None
        caller = None
        dut = RADIX(mem, caller)

        mask_size=4
        pgbase = SelectableInt(0,56)
        addrsh = SelectableInt(0,16)
        ret = dut._get_pgtable_addr(mask_size, pgbase, addrsh)
        log("ret=", ret)
        self.assertEqual(ret, 0, "pgtbl_addr should be 0")

    def test_walk_tree_1(self):

        # test address as in
        # https://github.com/power-gem5/gem5/blob/gem5-experimental/src/arch/power/radix_walk_example.txt#L65
        testaddr = 0x1000
        expected = 0x1000

        # starting prtbl
        prtbl = 0x1000000

        # set up dummy minimal ISACaller
        spr = {'DSISR': SelectableInt(0, 64),
               'DAR': SelectableInt(0, 64),
               'PIDR': SelectableInt(0, 64),
               'PRTBL': SelectableInt(prtbl, 64)
        }
        # set problem state == 0 (other unit tests, set to 1)
        msr = SelectableInt(0, 64)
        msr[MSRb.PR] = 0
        class ISACaller: pass
        caller = ISACaller()
        caller.spr = spr
        caller.msr = msr

        shift = SelectableInt(5, 6)
        mask = genmask(shift, 43)
        log ("    mask", bin(mask.value))

        mem = Mem(row_bytes=8, initial_mem=testmem)
        mem = RADIX(mem, caller)
        # -----------------------------------------------
        # |/|RTS1|/|     RPDB          | RTS2 |  RPDS   |
        # -----------------------------------------------
        # |0|1  2|3|4                55|56  58|59     63|
        data = SelectableInt(0, 64)
        data[1:3] = 0b01
        data[56:59] = 0b11
        data[59:64] = 0b01101 # mask
        data[55] = 1
        (rts, mbits, pgbase) = mem._decode_prte(data)
        log ("    rts", bin(rts.value), rts.bits)
        log ("    mbits", bin(mbits.value), mbits.bits)
        log ("    pgbase", hex(pgbase.value), pgbase.bits)
        addr = SelectableInt(0x1000, 64)
        check = mem._segment_check(addr, mbits, shift)
        log ("    segment check", check)

        log("walking tree")
        addr = SelectableInt(testaddr,64)
        # pgbase = None
        mode = None
        #mbits = None
        shift = rts
        result = mem._walk_tree(addr, mode)
        log("     walking tree result", result)
        log("should be", testresult)
        self.assertEqual(result.value, expected,
                             "expected 0x%x got 0x%x" % (expected,
                                                    result.value))

    def test_walk_tree_2(self):

        # test address slightly different
        testaddr = 0x1101
        expected = 0x5001101

        # starting prtbl
        prtbl = 0x1000000

        # set up dummy minimal ISACaller
        spr = {'DSISR': SelectableInt(0, 64),
               'DAR': SelectableInt(0, 64),
               'PIDR': SelectableInt(0, 64),
               'PRTBL': SelectableInt(prtbl, 64)
        }
        # set problem state == 0 (other unit tests, set to 1)
        msr = SelectableInt(0, 64)
        msr[MSRb.PR] = 0
        class ISACaller: pass
        caller = ISACaller()
        caller.spr = spr
        caller.msr = msr

        shift = SelectableInt(5, 6)
        mask = genmask(shift, 43)
        log ("    mask", bin(mask.value))

        mem = Mem(row_bytes=8, initial_mem=testmem2)
        mem = RADIX(mem, caller)
        # -----------------------------------------------
        # |/|RTS1|/|     RPDB          | RTS2 |  RPDS   |
        # -----------------------------------------------
        # |0|1  2|3|4                55|56  58|59     63|
        data = SelectableInt(0, 64)
        data[1:3] = 0b01
        data[56:59] = 0b11
        data[59:64] = 0b01101 # mask
        data[55] = 1
        (rts, mbits, pgbase) = mem._decode_prte(data)
        log ("    rts", bin(rts.value), rts.bits)
        log ("    mbits", bin(mbits.value), mbits.bits)
        log ("    pgbase", hex(pgbase.value), pgbase.bits)
        addr = SelectableInt(0x1000, 64)
        check = mem._segment_check(addr, mbits, shift)
        log ("    segment check", check)

        log("walking tree")
        addr = SelectableInt(testaddr,64)
        # pgbase = None
        mode = None
        #mbits = None
        shift = rts
        result = mem._walk_tree(addr, mode)
        log("     walking tree result", result)
        log("should be", testresult)
        self.assertEqual(result.value, expected,
                             "expected 0x%x got 0x%x" % (expected,
                                                    result.value))


if __name__ == '__main__':
    unittest.main()
